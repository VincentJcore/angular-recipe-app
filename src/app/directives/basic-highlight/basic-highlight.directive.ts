import {Directive, ElementRef, OnInit} from '@angular/core';

/**
 *  demonstration of attribute directive
 */
@Directive( {

  selector: '[appBasicHighLight]'

})

export class BasicHighlightDirective implements OnInit {

    constructor(private elementRef: ElementRef) {}


    ngOnInit(): void {

      this.elementRef.nativeElement.style.backgroundColor = 'green';
    }

}
