import {Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})

/**
 * Add directive to an html element and on click the class show is added
 */
export class DropdownDirective {

  @HostBinding('class.show') isOpen = false;

  @HostListener('click') toggleOpen() {

    console.log('the directive is listening');

    this.isOpen = !this.isOpen;
  }

}
