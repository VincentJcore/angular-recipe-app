import {Directive, HostBinding, HostListener, Input, OnInit} from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})

export class BetterHighlightDirective implements OnInit {

  @Input('appBetterHighlight') defaultColor = 'transparent';
  @Input() highlightColor = 'blue';
  @Input() reactionColor = 'red';

  @HostBinding('style.backgroundColor') backgroundColor = 'transparent';

  constructor() {}

  ngOnInit() {

    this.backgroundColor = this.defaultColor;

  }

  @HostListener('mouseenter') mouseover() {

    this.backgroundColor = this.highlightColor;
  }

  @HostListener('mouseleave') mouseleave() {

    this.backgroundColor = this.reactionColor;
    setTimeout(() => this.backgroundColor = this.defaultColor , 1000);

  }

}
