import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Ingredient} from '../../shared/ingredient.model';
import {ShoppingListService} from '../shopping-list.service';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {

  // @ts-ignore
  @ViewChild('amountInput') ingredientAmount: ElementRef;
  // @ts-ignore
  @ViewChild('nameInput') ingredientName: ElementRef;

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
  }

  addIngredient() {

    const ingredientName = this.ingredientName.nativeElement.value;
    const ingredientAmount = this.ingredientAmount.nativeElement.value;
    const ingredient = new Ingredient(ingredientName,  ingredientAmount);

    this.shoppingListService.addIngredient(ingredient);
  }



}
