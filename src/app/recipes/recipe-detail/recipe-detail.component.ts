import {Component, Input, OnInit} from '@angular/core';
import {Recipe} from '../recipe.model';
import {RecipeService} from '../recipe.service';
import {ActivatedRoute, ParamMap, Params, Router, RouterLinkActive} from '@angular/router';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {

  recipe: Recipe;
  id: number;

  constructor(private recipeService: RecipeService, private currentRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    const id = 'id';

    this.currentRoute.params.subscribe( (params: Params) => {
      this.id = +params[id];
      this.recipe = this.recipeService.getRecipe(this.id);
    });
  }

  onToShoppingList() {
    this.recipeService.addIngredientsToShoppingList(this.recipe.ingredients);
  }

  onEditRecipe() {
    this.router.navigate(['edit'], {relativeTo: this.currentRoute });
  }
}
