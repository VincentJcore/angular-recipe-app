import {EventEmitter, Injectable, Output} from '@angular/core';
import {ShoppingListService} from '../shopping-list/shopping-list.service';
import {Recipe} from './recipe.model';
import {Ingredient} from '../shared/ingredient.model';

@Injectable()
export class RecipeService {

  recipeSelected = new EventEmitter<Recipe>();

  private recipes: Recipe[] = [

    new Recipe('Toast'
      , 'Toast is great'
      , 'https://static.independent.co.uk/s3fs-public/thumbnails/image/' +
      '2018/03/14/14/bread-waste.jpg?w968h681'
      , [new Ingredient('bread', 42)]),
    new Recipe('Jam'
      , 'Jam is great'
      , 'https://assets.kraftfoods.com/recipe_images/opendeploy/179127_640x428.jpg'
      , [new Ingredient('sugar', 1), new Ingredient('fruit', 1)])

  ];

  constructor(private shoppingListService: ShoppingListService) { }

  /**
   * returns a copy of the recipe array
   */
  getRecipes() {

    return this.recipes.slice();
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {

    this.shoppingListService.addIngredients(ingredients);
  }

  getRecipe(index: number): Recipe {
    return this.recipes[index];
  }
}
