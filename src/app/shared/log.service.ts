/**
 * demonstrating a service with DI,
 * services are prototype by default
 */
export class LogService {

  log(message: string) {

    console.log(message);

  }
}
